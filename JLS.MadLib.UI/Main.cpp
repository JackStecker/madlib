#include <iostream>
#include <conio.h>
#include <string>
#include <fstream>

using namespace std;


void MadLib();


int main(){
	char run;

	
	cout << "do you wish to write a madlib. \n" << "Y or N. \n";
	cin >> run;
	cin.clear();
	cin.ignore();
	while (run == 'y' || run == 'Y') {
		MadLib();
		cin >> run;
		cin.clear();
		cin.ignore();
	}
	
	_getch();
	return 0;
}

void MadLib()
{

	char save;
	const int MAD_LIB = 12;
	string MadLibInfo[MAD_LIB];
	string MadLibScript[MAD_LIB] = {
		"an adjective (descriptive word): ","a sport: ",
		"a city: ","a person: ",
		"an action: ","a vehicle: ",
		"a place: ","a noun: ",
		"an adjective (descriptive word): ","a food: ",
		"a liquid: ","an adjective (descriptive word): "
	};

	for (int i = 0; i < MAD_LIB; i++) {
		
		cout << MadLibScript[i];
		getline(cin,MadLibInfo[i]);
		cout << "\n";
	}

	cout << "One day my " << MadLibInfo[0] << " friend and I decided to go to the " << MadLibInfo[1] << " game in " << MadLibInfo[2] << ". \n We really wanted to see " << MadLibInfo[3]
		<< " play. \n So we " << MadLibInfo[4] << " in the " << MadLibInfo[5] << " and headed down to the " << MadLibInfo[6] << " and bought some " << MadLibInfo[7] << ". \n We watched the game and it was "
		<< MadLibInfo[8] << ". \n We ate some " << MadLibInfo[9] << " and drank some " << MadLibInfo[10] << ". \n We had a " << MadLibInfo[11] << " time, and can't wait to go again. \n";

	cout << "do you wish to save. \n" << "Y or N. \n";
	cin >> save;
	cin.clear();
	cin.ignore();
	if (save == 'y' || save == 'Y') {
		string path = "C:\\Users\\Public\\test.txt";
		ofstream ofs(path);
		ofs<< "One day my " << MadLibInfo[0] << " friend and I decided to go to the " << MadLibInfo[1] << " game in " << MadLibInfo[2] << ". \n We really wanted to see " << MadLibInfo[3]
			<< " play. \n So we " << MadLibInfo[4] << " in the " << MadLibInfo[5] << " and headed down to the " << MadLibInfo[6] << " and bought some " << MadLibInfo[7] << ". \n We watched the game and it was "
			<< MadLibInfo[8] << ". \n We ate some " << MadLibInfo[9] << " and drank some " << MadLibInfo[10] << ". \n We had a " << MadLibInfo[11] << " time, and can't wait to go again. \n";
		ofs.close();

	}
	cout << "do you wish to go agian? \n" << "Y or N. \n";
}




